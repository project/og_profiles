
INTRO
--------------------------------------------------

This module integrates with the core Profile module and manages user profile fields at the group level.

Group admins can define fields that the user can fill in. Users must fill in required fields when they register in a group context or when join they group. All other core Profile functionality is not touched.

Displayed user profile fields will also be restricted to the groups that the user belongs to, or by detecting the user's group context.

Profile fields not associated with a group must be filled out by all users (e.g. first name, last name).

The next update to OG Profiles will be vastly different than the last commit, as it integrates with core Profile rather than modifying it. There will be an upgrade path.

INSTALL
--------------------------------------------------

Enable the module :)