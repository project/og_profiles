<?php

/**
 * @file
 * Organic groups profiles module.
 */

/**
 * Implementation of hook_menu().
 */
function og_profiles_menu() {
  $items = array();

  $items['node/%node/profiles'] = array(
    'title' => 'Profiles',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_admin_overview'),
    'access callback' => 'og_profiles_administer_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'profile.admin.inc',
    'file path' => drupal_get_path('module','profile'),
  );

  $items['node/%node/profiles/add/%'] = array(
    'title' => 'Add field',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_field_form', 4),
    'access callback' => 'og_profiles_administer_access',
    'access arguments' => array(1),
    'file' => 'profile.admin.inc',
    'file path' => drupal_get_path('module','profile'),
  );

  $items['node/%node/profiles/edit/%'] = array(
    'title' => 'Edit field',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_field_form', 4),
    'access callback' => 'og_profiles_check_field_ownership',
    'access arguments' => array(1, 4),
    'file' => 'profile.admin.inc',
    'file path' => drupal_get_path('module','profile'),
  );

  $items['node/%node/profiles/delete/%'] = array(
    'title' => 'Delete field',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('profile_field_delete', 4),
    'access callback' => 'og_profiles_check_field_ownership',
    'access arguments' => array(1, 4),
    'file' => 'profile.admin.inc',
    'file path' => drupal_get_path('module','profile'),
  );

  return $items;
}

/**
 * Check that the field being requested belongs to the group.
 *
 * @param $group
 *   A group node.
 * @param $fid
 *   A core profile field ID.
 *
 * @return
 *   TRUE if the user has access to edit this group profile field, FALSE otherwise.
 */
function og_profiles_check_field_ownership($group, $fid) {
  return og_is_group_admin($group) && db_result(db_query('select 1 from {og_profiles_fields} where gid = %d and fid = %d', $group->nid, $fid));
}

/**
 * Only show link if user is a group administrator.
 *
 * @param $group
 *   A group node.
 *
 * @return
 *   TRUE if the user has access to edit this group profile, FALSE otherwise.
 */
function og_profiles_administer_access($group) {
  return og_is_group_admin($group);
}

/**
 * Implementation of hook_form_alter().
 *
 * Modify profile field form to hide fields not belonging to this group.
 */
function og_profiles_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  $pattern = '#[/\w\-_]*admin/user/profile#U';

  if ($form_id == 'og_confirm_subscribe') {
    profile_load_profile($user);
    // If joining a group, show required fields.
    $fields = profile_form_profile((array)$user, $user, 'Profile');

    // Merge fields with the OG form.
    $form = array_merge($fields, $form);

    // Reorder so buttons are on bottom.
    $form['Profile']['#weight'] = 1;
    $form['actions']['#weight'] = 2;

    drupal_alter('form', $form, $form_state, 'user_register');

    array_unshift($form['#submit'], 'og_profile_save_profile');
  }

  if ($form_id == 'user_register') {
    $gid = og_get_group_context()->nid;
    $show_fields = _og_profiles_group_fields(TRUE);
    if ($gid) {
      // If registering in the context of a group, show global + group fields.
      foreach (element_children($form['Profile']) as $key => $val) {
        if (!in_array($val, $show_fields)) {
          $form['Profile'][$val]['#access'] = FALSE;
          $form['Profile'][$val]['#required'] = FALSE;
        }
      }
    }
    else {
      // If registering outside the context of a group, show global fields only.
      foreach (element_children($form['Profile']) as $key => $val) {
        if (!in_array($val, $show_fields)) {
          $form['Profile'][$val]['#access'] = FALSE;
          $form['Profile'][$val]['#required'] = FALSE;
        }
      }
    }
  }

  if ($form_id == 'profile_field_form' && $gid = og_get_group_context()->nid) {
    $form['fields']['category']['#access'] = FALSE;
    $form['fields']['category']['#default_value'] = 'Profile';
    $form['fields']['name']['#access'] = FALSE;
    $form['fields']['name']['#default_value'] = 'profile_'. uniqid();
    // Don't add submit handler if editing field.
    if (!$form['fid']) {
      $form['#submit'][] = 'og_profiles_field_form_submit';
    }
    else {
      $form['#submit'][] = 'og_profiles_field_form_edit_submit';
    }
  }

  if ($form_id == 'user_profile_form' && $gid = og_get_group_context()->nid) {
    $show_fields = _og_profiles_group_fields(TRUE);
    if (is_array($form['Profile'])) {
      foreach (element_children($form['Profile']) as $key => $val) {
        if (!in_array($val, $show_fields)) {
          $form['Profile'][$val]['#access'] = FALSE;
          $form['Profile'][$val]['#required'] = FALSE;
        }
      }
    }
    // Replace with our own validation.
    $form['#validate'] = array('og_profiles_validate_profile');
  }

  if ($form_id == 'profile_admin_overview') {
    if ($gid = og_get_group_context()->nid) {
      // If in the context of a group, restrict fields.
      $show_fields = _og_profiles_group_fields();
      $rows = element_children($form);
      foreach ($rows as $key => $val) {
        if ($val > 0) {
          if (!isset($show_fields[$val])) {
            // We have to unset it here, it will still show up as a row with #access = FALSE.
            unset($form[$val]);
          }
          else {
            $form[$val]['edit']['#value'] = l('Edit', "node/$gid/profiles/edit/$val");
            $form[$val]['delete']['#value'] = l('Delete', "node/$gid/profiles/delete/$val");
          }
        }
      }
      $form['addnewfields']['#value'] = preg_replace($pattern, url("node/$gid/profiles"), $form['addnewfields']['#value']);
    }
    else {
      // If in admin panel, hide group fields.
      $sql = "SELECT opf.fid, pf.name FROM {og_profiles_fields} opf
      INNER JOIN {profile_fields} pf ON pf.fid = opf.fid";
      $result = db_query($sql);
      while ($row = db_fetch_object($result)) {
        $show_fields[$row->fid] = $row->name;
      }
      $rows = element_children($form);
      foreach ($rows as $key => $val) {
        if ($val > 0) {
          if (isset($show_fields[$val])) {
            // We have to unset it here, it will still show up as a row with #access = FALSE.
            unset($form[$val]);
          }
        }
      }
    }
  }

  if ($form_id == 'profile_field_delete' && $gid = og_get_group_context()->nid) {
    $form['description']['#value'] = "<p>This action cannot be undone. If users have entered values into this field in their profile, these entries will also be deleted.</p>";
    $form['actions']['cancel']['#value'] = l('Cancel', "node/$gid/profiles");
    $form['#submit'][] = 'og_profiles_field_delete_submit';
  }
}

/**
 * Implementation of hook_user().
 *
 * @see profile_user().
 */
function og_profiles_user($type, &$edit, &$user, $category = NULL) {
  switch ($type) {
    case 'validate':
      return og_profiles_validate_profile($edit, $category);
    case 'view':
      // Restrict to global + group fields.
      $show_fields = _og_profiles_group_fields(TRUE);
      foreach(element_children($user->content['Profile']) as $key => $val) {
        if (!in_array($val, $show_fields)) {
          unset($user->content['Profile'][$val]);
        }
      }
  }
}

/**
 * @see profile_validate_profile().
 */
function og_profiles_validate_profile($edit, $category) {
  return TRUE;
}

/**
 * Send user back to group profile page.
 */
function og_profiles_field_delete_submit() {
  $gid = og_get_group_context()->nid;
  drupal_goto("node/$gid/profiles");
}

/**
 * Record field ID to associate field with group.
 */
function og_profiles_field_form_submit(&$form, &$form_state) {
  $record->fid = db_result(db_query('select max(fid) from {profile_fields}'));
  $record->gid = og_get_group_context()->nid;
  drupal_write_record('og_profiles_fields', $record);
  $sql = "update {profile_fields} set name = '%s' where fid = %d";
  db_query($sql, "profile_g_{$record->gid}_f_{$record->fid}", $record->fid);
  drupal_goto("node/{$record->gid}/profiles");
}

function og_profile_save_profile(&$form, &$form_state) {
  global $user;
  profile_save_profile($form_state['values'], $user, 'Profile');
}

function og_profiles_field_form_edit_submit(&$form, &$form_state) {
  $gid = og_get_group_context()->nid;
  drupal_goto("node/{$gid}/profiles");
}

/**
 * Get a list of fields the user should have to fill out.
 *
 * @param $global
 *   Whether or not to include global fields.
 */
function _og_profiles_group_fields($global = false) {
  $group = og_get_group_context();
  $show_fields = array();

  // Get fields that are global or in the group.
  $sql = "select pf.fid, pf.name, opf.gid from {profile_fields} pf
  left join {og_profiles_fields} opf on (pf.fid = opf.fid)
  where gid = %d or gid is null order by weight asc";
  $result = db_query($sql, $group->nid);
  while ($row = db_fetch_object($result)) {
    if ($row->gid > 0 || ($global && $row->gid === NULL)) {
      $show_fields[$row->fid] = $row->name;
    }
  }
  return $show_fields;
}
